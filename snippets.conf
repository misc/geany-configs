#
# EFL snippets definition for the Geany editor
#
# Usage:
#
# If you do NOT have other custom snippets:
#    Copy this file to "~/.config/geany/" (or just link it)
#
# Otherwise you need to copy/paste the snippets here to your file.
#
# NOTE: default snippets file (with docs) at: /usr/share/geany/snippets.conf
#
# NOTE: the final snippets.conf file is generated from the
#       snippets.multiline.conf one. Just run the snippets_multiline_parser.py
#       script to regenerate.
#

[Python]
### generic python
prop=@property\ndef %cursor%(self):\n\treturn\n
class=class %cursor%(object):\n\tdef __init__(self):\n\t\tpass\n

# Python-EFL
expand=size_hint_expand=EXPAND_BOTH
fill=size_hint_fill=FILL_BOTH
align=size_hint_align=(0.0,0.5)

[Cython]
# Sphyix
since=.. versionadded:: 1.%cursor%

# Python-EFL
propbool=property %cursor%:\n\t""" TODOC\n\n\tTODOC\n\n\t:type: TODOC\n\n\t.. versionadded:: TODOC\n\n\t"""\n\tdef __get__(self):\n\t\treturn bool(XXXXXX_get(self.obj))\n\n\tdef __set__(self, bint value):\n\t\tXXXXXX_set(self.obj, value)\n\ndef XXXXXX_get(self):\n\treturn bool(XXXXXX_get(self.obj))\ndef XXXXXX_set(self, bint value):\n\tXXXXXX_set(self.obj, value)\n


[Edje]
# blocks
collections=collections {\n\t%cursor% \n}
group=group { name: "%cursor%";\n\tparts {\n\t}\n}
parts=parts {\n\t%cursor% \n}
rel1=rel1 {\n\tto: "%cursor%";\n\trelative: 0.0 0.0;\n\toffset: 0 0;\n}
rel2=rel2 {\n\tto: "%cursor%";\n\trelative: 1.0 1.0;\n\toffset: -1 -1;\n}
fill=fill {\n\tsmooth: 1;\n\ttype: TILE;\n\torigin {\n\t\trelative: %cursor%0.0 0.0;\n\t\toffset: 0 0;\n\t}\n\tsize {\n\t\trelative: 1.0 1.0;\n\t\toffset: -1 -1;\n\t}\n}
origin=origin {\n\trelative: 0.0 0.0;\n\toffset: 0 0;\n}
text=text {\n\ttext: "";\n\tfont: "%cursor%";\n\tsize: 12;\n\ttext_class: "";\n\tmin: 1 0;\n\tellipsis: -1;\n}
script=script {\n\t%cursor%\n}

# parts
part=part { name: "%cursor%"; type: RECT;\n\tdescription { state: "default" 0.0;\n\t\t\n\t}\n}
RECT=part { name: "%cursor%"; type: RECT;\n\tdescription { state: "default" 0.0;\n\t}\n}
IMAGE=part { name: "%cursor%"; type: IMAGE;\n\tdescription { state: "default" 0.0;\n\t\timage {\n\t\t\tnormal: "";\n\t\t\tborder: left right top bottom;\n\t\t}\n\t}\n}

# colors
color_class=color_class {\n\tname: "%cursor%";\n\tcolor: 200 200 200 255;\n\tcolor2: 200 200 200 255;\n\tcolor3: 200 200 200 255; \n}

# text styles
styles=styles {\n\t%cursor%\n}
style=style {\n\tname: "%cursor%";\n\tbase: "font=Sans font_size=12 color=#fff wrap=word";\n\ttag: "b" "+ font_weight=Bold";\n}
base=base: "font=%cursor%Sans font_size=12 color=#fff wrap=word";
tag=tag: "%cursor%b" "+ font_weight=Bold";

# externals
externals=externals {\n\texternal: "%cursor%";\n}
external=external: "%cursor%";

# sounds
sounds=sounds {\n\t%cursor%\n}
sample=sample {\n\tname: "%cursor%" COMP;\n\tsource: "sound_file.wav";\n}
tone=tone: "%cursor%" 440;
vibrations=vibrations {\n\t%cursor% \n}

# images
images=images {\n\timage: "%cursor%.png" COMP;\n}
image=image: "%cursor%.png" COMP;
border=border: %cursor%left right top bottom;
size=size: %cursor%minw minh maxw maxh;
border_scale_by=border_scale_by: %cursor%0.0;
set=set {\n\tname: "%cursor%";\n\timage {\n\t\timage: "filename1.png" LOSSY 90;\n\t\tsize: minw minh maxw maxh;\n\t}\n\timage {\n\t\timage: "filename2.png" LOSSY 90;\n\t\tsize: minw minh maxw maxh;\n\t}\n}

# fonts
fonts=fonts {\n\tfont: "%cursor%.otf" "FontAlias";\n}
font=font: "%cursor%.otf" "FontAlias";


# data
data=data {\n\titem: "%cursor%key" "value";\n\tfile: "key" "filename.ext"\n}
item=item: "%cursor%key" "value";
file=file: "%cursor%key" "filename.ext";

# collections
base_scale=base_scale: %cursor%1.0;

# group
inherit_only=inherit_only: %cursor%1;
target_group=target_group: "%cursor%" "%cursor%" "%cursor%";
script_only=script_only: %cursor%on;
script_recursion=script_recursion: %cursor%1;
alias=alias: "%cursor%";
min=min: %cursor%width height;
max=max: %cursor%width height;
broadcast_signal=broadcast_signal: on;
orientation=orientation: LTR;
program_source=program_source: "%cursor%";
# group.limits
limits=limits {\n\tvertical: "%cursor%" height_barrier;\n\thorizontal: "limit_name" width_barrier;\n}
vertical=vertical: "%cursor%" height_barrier;
horizontal=horizontal: "%cursor%" width_barrier;

# part
type=type: %cursor%RECT;
program_remove=program_remove: "%cursor%" "%cursor%" "%cursor%";
part_remove=part_remove: "%cursor%" "%cursor%" "%cursor%";
physics_body=%cursor%RIGID_BOX;
insert_before=insert_before: "%cursor%";
insert_after=insert_after: "%cursor%";
mouse_events=mouse_events: 0;\n
repeat_events=repeat_events: 1;\n
scale=scale: 1;\n
ignore_flags=ignore_flags: ON_HOLD;\n
pointer_mode=pointer_mode: NOGRAB;\n
precise_is_inside=precise_is_inside: 1;\n
use_alternate_font_metrics=use_alternate_font_metrics: 1;\n
clip_to=clip_to: "%cursor%";
source=source: "%cursor%";
source2=source2: "%cursor%";
source3=source3: "%cursor%";
source4=source4: "%cursor%";
source5=source5: "%cursor%";
source6=source6: "%cursor%";
effect=effect: %cursor%SHADOW;
entry_mode=entry_mode: %cursor%EDITABLE;
select_mode=select_mode: %cursor%EXPLICIT;
cursor_mode=cursor_mode: %cursor%BEFORE;
multiline=multiline: 0;\n
access=access: 1;\n

name=name: "%cursor%";
color=color: 200 200 200 255;
color2=color2: 200 200 200 255;
color3=color3: 200 200 200 255;

# description
description=description { state: "%cursor%" 0.0;\n\t\n}
desc=description { state: "%cursor%" 0.0;\n\t\n}
inherit=inherit: "%cursor%default" 0.0;
state=state: "%cursor%" 0.0;
visible=visible: 0;\n
limit=limit: %cursor%;
align=align: %cursor%0.0 0.0;
fixed=fixed: 1 1;\n
minmul=minmul: %cursor%1.0 1.0;
step=step: %cursor%0 0;
aspect=aspect: %cursor%vmin vmax;\naspect_preference: BOTH;
aspect_preference=aspect_preference: BOTH;\n
offset=offset: %cursor%0 0;
to=to: "%cursor%";
to_x=to_x: "%cursor%";
to_y=to_y: "%cursor%";
normal=normal: "%cursor%.png" COMP;
tween=tween: "%cursor% .png";
middle=middle: NONE;\n
border_scale=border_scale: 1;\n
scale_hint=scale_hint: %cursor%STATIC;
ellipsis=ellipsis: -1;
repch=repch: "%cursor%*";
size_range=size_range: 1.2 1.2;
fit=fit: 1 0;
text_source=text_source: "%cursor%";
filter=filter: "%cursor%";

# dragable
dragable=dragable {\n\tconfine: "%cursor%";\n\tevents: "another dragable part";\n\tx: 0 0 0;\n\ty: 0 0 0;\n}
x=x: 1 0 0;\n
y=y: 1 0 0;\n
confine=confine: "%cursor%";
threshold=threshold: "%cursor%";
events=events: "%cursor%";
relative=relative: %cursor%0.0 0.0;

# box/table
box=box {\n\tlayout: "%cursor%vertical";\n\tpadding: 0 2;\n\talign: 0.5 0.5;\n\tmin: 0 0;\n\titems {\n\t\titem {\n\t\t\ttype: GROUP;\n\t\t\tsource: "";\n\t\t}\n\t}\n}
table=table {\n\thomogeneous: TABLE;\n\tpadding: 0 2;\n\talign: 0.5 0.5;\n\tmin: 0 0;\n\titems {\n\t\t%cursor%\n\t}\n}
layout=layout: "%cursor%";
homogeneous=homogeneous: %cursor%TABLE;
padding=padding: %cursor%0 0;
# box/table items
spread=spread: %cursor%width height;
prefer=prefer: %cursor%width height;
# padding=padding: %cursor%left right top bottom; (collide)
position=position: %cursor%col row;
span=span: %cursor%col row;
weight=weight: %cursor%x_val y_val;

# map
center=center: "%cursor%";
alpha=alpha: 1;
perspective_on=perspective_on: 1;
smooth=smooth: 1;
on=on: 1;
light=light: "%cursor%";
perspective=perspective: "%cursor%";
center=center: "%cursor%";
backface_cull=backface_cull: 1;
perspective=perspective {\n\tzplane: 0;\n\tfocal: 1000;\n}
zplane=zplane: %cursor%;
focal=focal: %cursor%;

# physics (obj)
physics=physics {\n\tignore_part_pos: 1;\n\tmass: %cursor%5.31;\n\tfriction: 0.5;\n\trestitution: 0.82;\n\tdamping: 0.4 0.24;\n\tsleep: 32 18.9;\n\tmaterial: IRON;\n\tdensity: 3.2;\n\thardness: 0.42;\n\tlight_on: 1;\n\tz: -15;\n\tdepth: 30;\n\tmovement_freedom { }\n\tfaces { }\n}
mass=mass: %cursor%;
restitution=restitution: %cursor%;
friction=friction: %cursor%;
ignore_part_pos=ignore_part_pos: %cursor%0;
damping=damping: %cursor%linear angular;
sleep=sleep: %cursor%linear angular;
material=material: %cursor%CONCRETE;
density=density: %cursor%;
hardness=hardness: %cursor%;
light_on=light_on: 1;
z=z: %cursor%;
depth=depth: %cursor%;
backface_cull=backface_cull: 1;
movement_freedom=movement_freedom {\n\tlinear: %cursor%1 1 0;\n\tangular: 0 0 1;\n}
linear=linear: %cursor%1 1 0;
angular=angular: %cursor%0 0 1;
faces=faces {\n\tface {\n\t\ttype: BOX_FRONT;\n\t\tsource: "%cursor%";\n\t}\n}
face=face {\n\ttype: BOX_FRONT;\n\tsource: "%cursor%";\n}
# physics (world)
world=world {\n\tgravity: 0 294 0;\n\trate: 30;\n\tz: -50;\n\tdepth: 100;\n}
gravity=gravity: %cursor%0 0 0;
rate=rate: %cursor%30;
depth=depth: %cursor%100;
#z=z: %cursor%-50; (collide)

# external params
params=params {\n\t%cursor%\n}
int=int: "%cursor%" 0;
double=double: "%cursor%" 0.0;
string=string: "%cursor%" "%cursor%";
bool=bool: "%cursor%" 1;
choice=choice: "%cursor%" "%cursor%";

# programs
link=link {\n\tbase: "edje,signal" "edje";\n\ttransition: LINEAR 0.2;\n\tin: 0.5 0.1;\n\tafter: "some_program";\n}
programs=programs {\n\tprogram { name: "%cursor%";\n\t\tsignal: "";\n\t\tsource: "";\n\t\taction: STATE_SET "default" 0.0;\n\t\ttransition: LINEAR 1.0;\n\t\ttarget: "";\n\t}\n}
program=program { name: "%cursor%";\n\tsignal: "";\n\tsource: "";\n\taction: STATE_SET "default" 0.0;\n\ttransition: LINEAR 1.0;\n\ttarget: "";\n}
prog=program { name: "%cursor%";\n\tsignal: "";\n\tsource: "";\n\taction: STATE_SET "default" 0.0;\n\ttransition: LINEAR 1.0;\n\ttarget: "";\n}
sequence=sequence {\n\t%cursor% \n}
signal=signal: "%cursor%";
source=source: "%cursor%";
filter=filter: "%cursor%" "%cursor%";
in=in: %cursor%1.0 0.0;
action=action: STATE_SET "%cursor%" 0.0;
transition=transition: LINEAR 1.0;
target=target: "%cursor%";
target_groups=target_groups: "%cursor%" "%cursor%";
after=after: "%cursor%";
api=api: "%cursor%" "%cursor%";
