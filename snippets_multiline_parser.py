#!/usr/bin/env python
#
# Simple parser to generate snippets.conf file from the snippets.multiline.conf
# template.
# In the template you can write multiline snippets, that will be single-lined
# by running this generator.
#
# Look at the existing template fiel to see the supported syntax.
#

TEMPLATE = 'snippets.multiline.conf'
OUTPUT = 'snippets.conf'

MODE_SEARCH = 0
MODE_INSIDE = 1

mode = MODE_SEARCH

with open(TEMPLATE,'r') as infile, open(OUTPUT,'w') as outfile:
    for line in infile:

        if mode == MODE_INSIDE:
            if line.startswith('# '):
                # print("+", line)
                escaped = line[2:]
                escaped = escaped.replace('\n', '\\n')
                escaped = escaped.replace('\t', '\\t')
                escaped = escaped.replace('    ', '\\t')
                outfile.write(escaped)
            else:
                # print("END", line)
                outfile.write('\n')
                mode = MODE_SEARCH

        if mode == MODE_SEARCH:
            if line.endswith('=MULTILINE\n'):
                # print("START", line)
                outfile.write(line.replace('MULTILINE\n', ''))
                mode = MODE_INSIDE
            else:
                outfile.write(line)

